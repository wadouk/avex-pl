// © Frederic Tapissier / Nicolas Betheuil
//import {
//    MapLibreSearchControl
//} from "https://unpkg.com/@stadiamaps/maplibre-search-box@1.0.1/dist/maplibre-search-box.mjs";

// constantes
// const STYLE_FOND_DE_CARTE_PAR_DEFAUT = 'https://api.maptiler.com/maps/basic-v2/style.json?key=Cib18YVHlgGgsXyYFzPN';

const STYLE_FOND_DE_CARTE_PAR_DEFAUT = {
    version: 8,
    sources: {
        "raster-tiles": {
            type: 'raster',
            tiles: [
                'https://a.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
                'https://b.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
                'https://c.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'
            ],
            tileSize: 256,
            attribution: "donn&eacute;es &copy; <a href=\"//osm.org/copyright\">OpenStreetMap</a>/ODbL - rendu <a href=\"//openstreetmap.fr\">OSM France</a>"
        }
    },
    layers: [
        {

            'id': 'simple-tiles',
            'type': 'raster',
            'source': 'raster-tiles',
            'minzoom': 1,
            'maxzoom': 20
        }
    ]
};


const ATTRIBUTION = '©AVEX // SIG calcul & rendu : frederic Tapissier 07 89 22 72 86 ftapissier@gmail.com || Artisan logiciel : Nicolas Bétheuil';

// gdalinfo -json carte\ france\ 2024\ sat\ visuel-export.tif | jq .wgs84Exten
// min [0], min [1] Sud ouest
// max [0], max [0] Nord est
const MAP_BOUNDS =
    [
        [-8.443299, 39.9493229],
        [11.3018505, 53.3964875]
    ]

// https://openmoji.org/library/emoji-1F4F7/#variant=black
const camera = `<svg viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
  <g id="line">
    <path fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m15.2697,22.9178c-1.7365,0-3.1441,1.371-3.1441,3.0622v23.2213c0,1.6912,1.4076,3.0623,3.1441,3.0623h41.6606c1.7365,0,3.1441-1.3711,3.1441-3.0623v-23.2213c0-1.6912-1.4076-3.0622-3.1441-3.0622,0,0-41.6606,0-41.6606,0Z"/>
    <line x1="47.1521" x2="59.6177" y1="33.1975" y2="33.1975" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <line x1="12.5823" x2="20.6288" y1="33.1975" y2="33.1975" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <circle cx="33.8524" cy="37.5907" r="10" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <circle cx="33.8524" cy="37.5907" r="5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <path fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m27.831,19.1198v-1.0782c0-.5638-.4692-1.0208-1.048-1.0208h-7.6766c-.5787,0-1.048.457-1.048,1.0208v1.0782"/>
  </g>
</svg>`

// https://openmoji.org/library/emoji-1F50D/#variant=black
const picker = `<svg viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
  <g id="line">
    <ellipse cx="29.5854" cy="24.8305" rx="14.6372" ry="14.6372" transform="matrix(0.8006 -0.5992 0.5992 0.8006 -8.979 22.6778)" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
    <ellipse cx="29.5854" cy="24.8305" rx="11.1656" ry="11.1657" transform="matrix(0.8006 -0.5992 0.5992 0.8006 -8.979 22.6777)" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
    <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M38.6805,41.7876l4.1839-3.1316l11.6927,15.622c0.8354,1.1162,0.5758,2.7219-0.5795,3.5867l0,0 c-1.1554,0.8647-2.7691,0.661-3.6045-0.4551L38.6805,41.7876z"/>
  </g>
</svg>`

// https://openmoji.org/library/emoji-1F30C/#variant=black
const milkyWay = `
<svg viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
  <g id="line-supplement">
    <g>
      <polygon stroke="#000" stroke-linejoin="round" points="53.75 17.19 54.677 17.853 54.333 16.767 55.25 16.09 54.11 15.991 53.75 15 53.39 16.081 52.25 16.09 53.167 16.767 52.823 17.853 53.75 17.19"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="0.75" points="55 27.271 55.618 27.713 55.389 26.989 56 26.538 55.24 26.532 55 25.811 54.76 26.532 54 26.538 54.611 26.899 54.382 27.713 55 27.271"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="2" points="47 24.838 48.236 25.811 47.777 24.363 49 23.46 47.481 23.449 47 22.007 46.519 23.449 45 23.46 46.223 24.363 45.764 25.811 47 24.838"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="1.5" points="43.103 44.882 44.403 45.811 43.92 44.288 45.206 43.339 43.608 43.327 43.103 41.811 42.598 43.237 41 43.339 42.285 44.288 41.803 45.811 43.103 44.882"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="2" points="52.25 34.214 53.486 35.098 53.027 33.649 54.25 32.747 52.731 32.645 52.25 31.294 51.769 32.735 50.25 32.747 51.473 33.649 51.014 35.098 52.25 34.214"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="2" points="42 36.192 43.854 37.517 43.166 35.344 45 33.991 42.721 33.884 42 31.811 41.279 33.974 39 33.991 40.834 35.344 40.146 37.517 42 36.192"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="2" points="35.75 49.506 37.45 50.811 36.819 48.819 38.5 47.578 36.411 47.563 35.75 45.58 35.089 47.563 33 47.578 34.681 48.819 34.05 50.811 35.75 49.506"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="0.75" points="35 41.369 35.618 41.811 35.389 41.087 36 40.636 35.24 40.63 35 39.909 34.76 40.63 34 40.636 34.611 41.087 34.382 41.721 35 41.369"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="0.75" points="48 38.271 48.618 38.713 48.389 37.989 49 37.538 48.24 37.442 48 36.811 47.76 37.532 47 37.538 47.611 37.989 47.382 38.713 48 38.271"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="0.75" points="29 52.369 29.618 52.811 29.389 52.087 30 51.636 29.24 51.63 29 50.909 28.76 51.63 28 51.636 28.611 52.087 28.382 52.721 29 52.369"/>
      <polygon stroke="#000" stroke-linejoin="round" points="16.5 57.001 17.427 57.664 17.083 56.578 18 55.811 16.86 55.892 16.5 54.811 16.14 55.892 15 55.901 15.917 56.578 15.573 57.664 16.5 57.001"/>
      <polygon stroke="#000" stroke-linejoin="round" points="27.5 48.001 28.427 48.664 28.083 47.578 29 46.901 27.86 46.803 27.5 45.811 27.14 46.892 26 46.901 26.917 47.578 26.573 48.664 27.5 48.001"/>
      <polygon stroke="#000" stroke-linejoin="round" stroke-width="1.5" points="22.103 53.882 23.403 54.811 22.92 53.288 24.206 52.339 22.608 52.327 22.103 50.811 21.598 52.327 20 52.339 21.285 53.198 20.803 54.811 22.103 53.882"/>
    </g>
  </g>
  <g id="line">
    <rect x="12" y="12" rx="1" width="48" height="48" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="2"/>
  </g>
</svg>`

const star = `<img width="29" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAdCAYAAABWk2cPAAACoUlEQVRIS+WWSahOYRjH7zVkHjIsrFjQzYqFJMXWUIZkVnwWhnDLXVwpGUopUYZMJfrIlAWyQZQFC5SSssGCSCSZp2v8/XROnU7nnO89utddOPXr/c77vuf5P8/zPu/7fvV17fDUt4NmXajoGJxbDtPhHByAG3/rcKhoFYFFCZEj/K60tegbBPokRN7yu29bi/7zSDsT0WhYklhTnbgFn+FX2YhrrakpnQyv4Rp8hJ4wFRrgGDwsK1wk6thM2AfPYD7cg0FwECbABVgJT8pEWyQ6EEM7YB7shvXwATrBCtgKHWEXbIRPocJ5ovZPA/eja7YQLieM6pDRToH3sApOwfcQ4TxRjW6D2ZGxZtpXKYMjeD8Jw+FB5NjNyMlC7TzRSVEkps+T6HyGsQ70zYqyYcFdgQq4/qVF3fRbYDGchSZ4kWOlK/1rYF00vpN2E7iVcp+sSMcz+zh0AQvmDPwssGE1HwKr+V30zWnaH3nfpEX1fAOsBT9shJdFXkdjXghHYRjcB9N+N1R0LBNPQD9YBlZkyInj+s6FveD6bgdT/i1LOBmp+28puCctHFP7PCDKeIpZMjPu56vgofG0lqge7odx4OlzPTDKpN0BvHiKeVD0APe69jxCzVrVyclITdFQ6A+3oaVElPFU7Rmxh4knlcUYP1/54farxqKreRFL3smKzwC3j+uiA3rvhx769tna9wU8lbxjXQ77LoLVnH4u0TExFn3Ey2B4DCPBdV0A8bjFJG4dcTuIzojCVrkVfxhcy+4Zon/SnhXpHgZGgYUwBHo7EUyb6fJ+tejEJdFGbMcr0KtwM9SMNO2Ux5+e9opaRbuBd6mtY6ITtjpiBu6ABTgHvCwK1zQt2hrvFYy4dwurtzWEgmzU+rsSZKTspP9H9DciEY8efZMCdAAAAABJRU5ErkJggg==">`

// https://openmoji.org/library/emoji-1F52D/#variant=black
const telescope = `<svg viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
  <g id="line">
    <polyline fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" points="16.0186 56.6623 29.1491 36.039 31.624 36.039 43.8948 56.6564"/>
    <line x1="30.4618" x2="30.4618" y1="36.741" y2="60.257" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <line x1="9.7475" x2="11.8352" y1="32.5748" y2="31.7117" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <polyline fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" points="45.6328 12.1786 48.8033 10.5598 52.3814 18.772 49.2019 19.9125"/>
    <line x1="26.8219" x2="30.3531" y1="19.924" y2="31.3607" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
    <circle cx="30.3531" cy="31.3607" r="2"/>
    <polyline fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" points="25.028 28.5329 16.1973 31.7319 14.6585 28.1744 23.5457 23.5568"/>
    <polyline fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" points="30.9216 19.7807 42.1281 14.0815 45.2549 21.3103 32.6562 25.8289"/>
  </g>
</svg>`

const maxIcon = `<svg viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg"><path transform="matrix(.06959 0 0 .06959 -30.807 -8.276)" d="m615.2 219.6 159.2 159.1M1145.6 750l159.2 159.1m-689.6 0L774.4 750m371.2-371.3 159.2-159.1M472.5 564.4h225m525 0h225.1" fill="none" stroke="#ffc500" stroke-linecap="round" stroke-linejoin="round" stroke-width="50"/><path transform="matrix(.06959 0 0 .06959 -30.804 -8.276)" d="M997.6 259c.119 71.935-117.234 36.373-76.828-23.484 21.25-34.295 78.427-16.917 76.828 23.484ZM762.3 470.2h66L960 301l131.7 169.2h66m-197.7 0v188.4h-93.5c-1.065-64.478-17.608-127.657-38.2-188.4h263.4c-20.593 60.743-37.135 123.922-38.2 188.4H960m-93.5-.1L960 760.2l93.5-101.7M960 760.2v294.9" fill="#ffc500" stroke="#231f20" stroke-linecap="round" stroke-linejoin="round" stroke-width="50"/></svg>`

const minIcon = `<svg viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg"><path transform="matrix(.06959 0 0 .06959 -30.804 -8.276)" d="M997.6 259c.119 71.935-117.234 36.373-76.828-23.484 21.25-34.295 78.427-16.917 76.828 23.484ZM762.3 470.2h66L960 301l131.7 169.2h66m-197.7 0v188.4h-93.5c-1.065-64.478-17.608-127.657-38.2-188.4h263.4c-20.593 60.743-37.135 123.922-38.2 188.4H960m-93.5-.1L960 760.2l93.5-101.7M960 760.2v294.9" fill="none" stroke="#231f20" stroke-linecap="round" stroke-linejoin="round" stroke-width="50"/></svg>`

const TUILES =
    [
        { label: "PL Minimum (sat black marble 2024)", url : 'black-marble-2024-512-webp/{z}/{x}/{y}.webp', icon: minIcon, id: 'avex-pl-visuel'},
        { label: "PL Maximum (corrinne 2018 visuel)", url : 'corinne-2018-min-512-webp/{z}/{x}/{y}.webp', icon: maxIcon, id: 'avex-pl-corine'},
        { label: "PL AstroPhoto  (corrinne 2018 photo)", url : 'corinne-2018-max-512-webp/{z}/{x}/{y}.webp', icon: camera, id: 'avex-pl-photo'},
    ]

function avex() {

    const map = new maplibregl.Map({
        container: 'map',
        style: STYLE_FOND_DE_CARTE_PAR_DEFAUT,
        hash: true,
        bounds: MAP_BOUNDS,

        // permet d'éviter les 404 sur les tuiles
        // et limiter le nombre de tuiles vectoriels demandées
        maxBounds: MAP_BOUNDS,

        preserveDrawingBuffer: true // nécessaire pour le color picker
    });

    map.addControl(new maplibregl.NavigationControl({
        visualizePitch: true
    }), 'top-right');

    map.addControl(new maplibregl.FullscreenControl());
    map.addControl(new maplibregl.GeolocateControl());
    map.on('load', () => {
        TUILES.forEach(({id, url, scheme}) => {
            map.addSource(id, {
                type: 'raster',
                tiles: [url],
                attribution: ATTRIBUTION
            })
            map.addLayer({id: id, type: 'raster', source: id})
        })

        class AvexLayerControl {
            #map = null
            #container = null

            #picker = false

            #layers = TUILES.map(({id, label, icon}) => [id, label, icon])


            #activeLayer = this.#layers[0][0]

            #wrapper =
                `<div class="avexLayerControl maplibregl-ctrl">
                    <div id="color" class="maplibregl-ctrl maplibregl-ctrl-group inactive">
                            <span id="starNb" title="Nombre d'étoiles visible (approximativement)"></span>
                            <span id="constellation" title="Dessin des constellations reconnaissable" class="inactive">${star}</span>
                            <span id="messiers" title="Principaux Messiers visibles" class="inactive">${telescope}</span>
                            <span id="vl" title="Voie lactée visible" class="inactive">${milkyWay}</span>
                            <button title="Mesure PL">${picker}</button>
<!--                            <span id="legendName" title="Niveau de PL"></span>-->
                        </div>
                    <div id="avex-layers-ctrl" class="maplibregl-ctrl maplibregl-ctrl-group expand">
                    <button class="expand"></button>
                    <ul class="layers">
                    </ul>
                    <input type="range" orient="vertical" step="10" min="10" max="100" class="opacity">
                    </div>
                </div>
                `
            #layer(layerid, layername, icon) {
                return `<div><button class="select${layerid}" alt="${layername}" title="${layername}">${icon}</button><label for="">${layername}</label></div>`
            }

            #import(fragment) {
                return new DOMParser().parseFromString(fragment, "text/html").documentElement.querySelector("body").firstElementChild
            }

            #change() {
                this.#layers
                    .forEach(([id, _]) => {
                        this.#map.setPaintProperty(id, 'raster-opacity', Number(this.#getOpacitySlider().value / 100));
                    })

            }

            #getOpacitySlider() {
                return this.#container.querySelector(".opacity")
            }
            #getLayers() {
                return this.#container.querySelector(".layers")
            }

            #click(event) {
                this.#layers
                    .forEach(([layerId]) => {
                        const visibility = event.currentTarget.className.indexOf(`select${layerId}`) === -1  ? 'none' : 'visible'
                        this.#map.setLayoutProperty(layerId, 'visibility', visibility)
                        this.#updateButton(layerId, this.#container.querySelector(`.select${layerId}`), visibility)
                    })
            }

            #updateButton(layerId, b, visib) {
                if (visib === 'visible') {
                    this.#activeLayer = layerId
                    b.classList.remove('inactive')
                    b.classList.add('active')
                } else {
                    b.classList.remove('active')
                    b.classList.add('inactive')
                }
            }

            onAdd(map) {
                this.#map = map;
                this.#container = this.#import(this.#wrapper)
                this.#layers
                    .map(([id, label, icon]) => [id, this.#import(this.#layer(id, label, icon))])
                    .forEach(([id, v]) => {
                        const visibility = this.#layers[0][0] === id ? 'visible' : 'none'
                        this.#map.setLayoutProperty(id, 'visibility', visibility)
                        const button = v.querySelector('button')
                        button.addEventListener("click", this.#click.bind(this))
                        const layers = this.#getLayers();
                        Array.from(v.childNodes).forEach(c => layers.appendChild(c))
                        this.#updateButton(id, button, visibility)
                    })
                this.#getOpacitySlider().addEventListener("input", this.#change.bind(this))

                this.#change()

                const colorEl = this.#container.querySelector("#color")

                colorEl.querySelector("button")
                    .addEventListener("click", e => {
                        this.#picker = !this.#picker
                        if(this.#picker) {
                            this.#getOpacitySlider().value = 100
                            this.#map.getCanvas().style.cursor = 'crosshair';

                            colorEl.classList.remove('inactive')

                            this.#change()
                        } else {
                            colorEl.classList.add('inactive')
                            this.#map.getCanvas().style.cursor = '';
                        }
                    })

                // https://stackoverflow.com/questions/57862383/how-to-get-the-color-of-pixels-on-mapbox
                map.on("click", e => {
                    const canvas = map.getCanvas();
                    const gl = canvas.getContext('webgl') || canvas.getContext('webgl2');
                    if (gl) {
                        const { point } = e;
                        const { x, y } = point;
                        const data = new Uint8Array(4);

                        // https://webglfundamentals.org/webgl/lessons/webgl-picking.html
                        const canvasX = x * gl.canvas.width / gl.canvas.clientWidth;
                        const canvasY = gl.canvas.height - y * gl.canvas.height / gl.canvas.clientHeight - 1;

                        gl.readPixels(canvasX, canvasY, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, data);
                        const [r, g, b, a] = data;
                        const [h, s, l] = rgbToHsl(r, g, b)
                        if (this.#picker) {
                            const { minStar, maxStar, constellation, messiers, vl, light2, hue2, color } = simplifyColor(h, l)
                            colorEl.querySelector("#starNb").innerText = `${minStar} - ${maxStar}`
                            colorEl.querySelector("#constellation").className = constellation
                            colorEl.querySelector("#messiers").className = messiers
                            colorEl.querySelector("#vl").className = vl
                            // colorEl.querySelector("#legendName").innerText = color
                        }
                    }
                });

                const avexLayersCtrl = this.#container.querySelector("#avex-layers-ctrl")

                this.#container.querySelector("button.expand").addEventListener('click', (e) => {
                    avexLayersCtrl.classList.toggle('expand')
                })

                map.on('drag', () => {
                    avexLayersCtrl.classList.remove('expand')
                })
                map.on('zoom', () => {
                    avexLayersCtrl.classList.remove('expand')
                })

                return this.#container;
            }

            onRemove() {
                this.#container.parentNode.removeChild(this.#container);
                this.#map = undefined;
            }

        }
        try {
            map.addControl(new AvexLayerControl(), 'bottom-right');
        } catch (e) {
            console.error("fail", e)
        }
        //       map.addControl(new MapLibreSearchControl(), 'top-left')
        map.addControl(new maplibregl.ScaleControl({
            maxWidth: window.innerWidth / 10
        }), 'bottom-left')
    })
}

//https://gist.github.com/mjackson/5311256
/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSL representation
 */
function rgbToHsl(r, g, b) {
    r /= 255, g /= 255, b /= 255;

    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max === min) {
        h = s = 0; // achromatic
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }

        h /= 6;
    }

    return [ h, s, l ];
}


// const legendColors = [
//     `rgb(255, 255, 255)`, // blanc    0 50
//     `rgb(255,   0, 255)`, // magenta 50 100 constellations
//     `rgb(255,   0,   0)`, // rouge  100 200 constellations messiers
//     `rgb(255, 153,   0)`, // orange 200 250
//     `rgb(255, 255,   0)`, // jaune  250 500 vl ?
//     `rgb(  0, 255,   0)`, // vert   500 1000 halos de pl
//     `rgb(  0, 204, 255)`, // cyan  1000 1800 vl
//     `rgb(  0,   0, 255)`, // bleu  1800 3000 vl ++
//     `rgb(  0,   0, 128)`, // bleu nuit 3000 5000
//     `rgb(  0,   0,   0)`] // noir PL = 0 au zenith

function simplifyColor(hue, light) {
    const light2 = Math.round(light * 2)
    const hue2 = Math.round(hue * 12) % 12

    /**
     * 2 constellation (bool)
     * 3 messiers (bool)
     * 4 vl (int 0 - 3)
     */
    const [minStar, maxStar, constellation, messiers, vl, color] = (() => {
        if (light2 === 0) {
            return ["3k", "6k", "active", "active", 3, "noir"]
        } else if (light2 === 2) {
            return ["0", "50", "inactive", "inactive", 0, "blanc"]
        } else
            return ([
                ["100", "200", "active", "active", 0, "rouge"],     // 0                0
                ["200", "250", "active", "active", 0, "orange"],    // 30               1
                ["250", "500", "active", "active", 1, "jaune"],     // 60               2
                ["500", "1k", "active", "active", 1, "vert"],       // 90               3
                ["500", "1k", "active", "active", 1, "vert"],       // 120              4
                ["500", "1k", "active", "active", 1, "vert"],       // 150              5
                ["1k", "~2k", "active", "active", 2, "cyan"],       // 180              6
                ["~2k", "3k", "active", "active", 3, "bleu"],       // 210              7
                ["~2k", "3k", "active", "active", 3, "bleu"],       // 240              8
                ["3k", "5k", "active", "active", 3, "bleu nuit"],   // 270              9
                ["50", "100", "active", "inactive", 0, "magenta"],  // 300              10
                ["50", "100", "active", "inactive", 0, "magenta"],  // 330              11
            ])[hue2]
    })()

    // console.log({hue2, light2, vl, color})

    return { minStar, maxStar, constellation, messiers, vl: `vl${vl}`, light2, hue2, color }
}

window.addEventListener('load', avex)
